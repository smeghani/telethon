from ....tl.mtproto_request import MTProtoRequest


class GetInviteTextRequest(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    help.getInviteText#4d392343  = help.InviteText"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x4d392343

    def __init__(self):
        super(GetInviteTextRequest, self).__init__()
        self.result = None
        self.confirmed = True  # Confirmed by default

    def on_send(self, writer):
        writer.write_int(GetInviteTextRequest.constructor_id, signed=False)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return GetInviteTextRequest()

    def on_response(self, reader):
        self.result = reader.tgread_object()

    def __repr__(self):
        return 'help.getInviteText#4d392343  = help.InviteText'

    def __str__(self):
        return '(help.getInviteText (ID: 0x4d392343) = ())'.format()
