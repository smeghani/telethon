from ....tl.mtproto_request import MTProtoRequest


class SaveAppLogRequest(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    help.saveAppLog#6f02f748 events:Vector<InputAppEvent> = Bool"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x6f02f748

    def __init__(self, events):
        """
        :param events: Telegram type: InputAppEvent. Must be a list.
        """
        super(SaveAppLogRequest, self).__init__()
        self.result = None
        self.confirmed = True  # Confirmed by default

        self.events = events

    def on_send(self, writer):
        writer.write_int(SaveAppLogRequest.constructor_id, signed=False)
        writer.write_int(0x1cb5c415, signed=False)  # Vector's constructor ID
        writer.write_int(len(self.events))
        for events_item in self.events:
            events_item.on_send(writer)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return SaveAppLogRequest(None)

    def on_response(self, reader):
        self.result = reader.tgread_object()

    def __repr__(self):
        return 'help.saveAppLog#6f02f748 events:Vector<InputAppEvent> = Bool'

    def __str__(self):
        return '(help.saveAppLog (ID: 0x6f02f748) = (events={}))'.format(None if not self.events else [str(_) for _ in self.events])
