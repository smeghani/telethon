from ....tl.mtproto_request import MTProtoRequest


class ImportCardRequest(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    contacts.importCard#4fe196fe export_card:Vector<int> = User"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x4fe196fe

    def __init__(self, export_card):
        """
        :param export_card: Telegram type: int. Must be a list.
        """
        super(ImportCardRequest, self).__init__()
        self.result = None
        self.confirmed = True  # Confirmed by default

        self.export_card = export_card

    def on_send(self, writer):
        writer.write_int(ImportCardRequest.constructor_id, signed=False)
        writer.write_int(0x1cb5c415, signed=False)  # Vector's constructor ID
        writer.write_int(len(self.export_card))
        for export_card_item in self.export_card:
            writer.write_int(export_card_item)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return ImportCardRequest(None)

    def on_response(self, reader):
        self.result = reader.tgread_object()

    def __repr__(self):
        return 'contacts.importCard#4fe196fe export_card:Vector<int> = User'

    def __str__(self):
        return '(contacts.importCard (ID: 0x4fe196fe) = (export_card={}))'.format(None if not self.export_card else [str(_) for _ in self.export_card])
