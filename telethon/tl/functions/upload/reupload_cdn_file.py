from ....tl.mtproto_request import MTProtoRequest


class ReuploadCdnFileRequest(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    upload.reuploadCdnFile#2e7a2020 file_token:bytes request_token:bytes = Bool"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x2e7a2020

    def __init__(self, file_token, request_token):
        """
        :param file_token: Telegram type: bytes.
        :param request_token: Telegram type: bytes.
        """
        super(ReuploadCdnFileRequest, self).__init__()
        self.result = None
        self.confirmed = True  # Confirmed by default

        self.file_token = file_token
        self.request_token = request_token

    def on_send(self, writer):
        writer.write_int(ReuploadCdnFileRequest.constructor_id, signed=False)
        writer.tgwrite_bytes(self.file_token)
        writer.tgwrite_bytes(self.request_token)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return ReuploadCdnFileRequest(None, None)

    def on_response(self, reader):
        self.result = reader.tgread_object()

    def __repr__(self):
        return 'upload.reuploadCdnFile#2e7a2020 file_token:bytes request_token:bytes = Bool'

    def __str__(self):
        return '(upload.reuploadCdnFile (ID: 0x2e7a2020) = (file_token={}, request_token={}))'.format(str(self.file_token), str(self.request_token))
