from ....tl.mtproto_request import MTProtoRequest


class GetSavedInfoRequest(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    payments.getSavedInfo#227d824b  = payments.SavedInfo"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x227d824b

    def __init__(self):
        super(GetSavedInfoRequest, self).__init__()
        self.result = None
        self.confirmed = True  # Confirmed by default

    def on_send(self, writer):
        writer.write_int(GetSavedInfoRequest.constructor_id, signed=False)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return GetSavedInfoRequest()

    def on_response(self, reader):
        self.result = reader.tgread_object()

    def __repr__(self):
        return 'payments.getSavedInfo#227d824b  = payments.SavedInfo'

    def __str__(self):
        return '(payments.getSavedInfo (ID: 0x227d824b) = ())'.format()
