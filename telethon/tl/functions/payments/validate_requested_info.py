from ....tl.mtproto_request import MTProtoRequest


class ValidateRequestedInfoRequest(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    payments.validateRequestedInfo#770a8e74 flags:# save:flags.0?true msg_id:int info:PaymentRequestedInfo = payments.ValidatedRequestedInfo"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x770a8e74

    def __init__(self, msg_id, info, save=None):
        """
        :param save: Telegram type: true.
        :param msg_id: Telegram type: int.
        :param info: Telegram type: PaymentRequestedInfo.
        """
        super(ValidateRequestedInfoRequest, self).__init__()
        self.result = None
        self.confirmed = True  # Confirmed by default

        self.save = save
        self.msg_id = msg_id
        self.info = info

    def on_send(self, writer):
        writer.write_int(ValidateRequestedInfoRequest.constructor_id, signed=False)
        # Calculate the flags. This equals to those flag arguments which are NOT None
        flags = 0
        flags |= (1 << 0) if self.save else 0
        writer.write_int(flags)

        writer.write_int(self.msg_id)
        self.info.on_send(writer)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return ValidateRequestedInfoRequest(None, None, None)

    def on_response(self, reader):
        self.result = reader.tgread_object()

    def __repr__(self):
        return 'payments.validateRequestedInfo#770a8e74 flags:# save:flags.0?true msg_id:int info:PaymentRequestedInfo = payments.ValidatedRequestedInfo'

    def __str__(self):
        return '(payments.validateRequestedInfo (ID: 0x770a8e74) = (save={}, msg_id={}, info={}))'.format(str(self.save), str(self.msg_id), str(self.info))
