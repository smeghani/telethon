from ....tl.mtproto_request import MTProtoRequest


class GetChannelDifferenceRequest(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    updates.getChannelDifference#03173d78 flags:# force:flags.0?true channel:InputChannel filter:ChannelMessagesFilter pts:int limit:int = updates.ChannelDifference"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x3173d78

    def __init__(self, channel, filter, pts, limit, force=None):
        """
        :param force: Telegram type: true.
        :param channel: Telegram type: InputChannel.
        :param filter: Telegram type: ChannelMessagesFilter.
        :param pts: Telegram type: int.
        :param limit: Telegram type: int.
        """
        super(GetChannelDifferenceRequest, self).__init__()
        self.result = None
        self.confirmed = True  # Confirmed by default

        self.force = force
        self.channel = channel
        self.filter = filter
        self.pts = pts
        self.limit = limit

    def on_send(self, writer):
        writer.write_int(GetChannelDifferenceRequest.constructor_id, signed=False)
        # Calculate the flags. This equals to those flag arguments which are NOT None
        flags = 0
        flags |= (1 << 0) if self.force else 0
        writer.write_int(flags)

        self.channel.on_send(writer)
        self.filter.on_send(writer)
        writer.write_int(self.pts)
        writer.write_int(self.limit)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return GetChannelDifferenceRequest(None, None, None, None, None)

    def on_response(self, reader):
        self.result = reader.tgread_object()

    def __repr__(self):
        return 'updates.getChannelDifference#03173d78 flags:# force:flags.0?true channel:InputChannel filter:ChannelMessagesFilter pts:int limit:int = updates.ChannelDifference'

    def __str__(self):
        return '(updates.getChannelDifference (ID: 0x3173d78) = (force={}, channel={}, filter={}, pts={}, limit={}))'.format(str(self.force), str(self.channel), str(self.filter), str(self.pts), str(self.limit))
