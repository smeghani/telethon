from ....tl.mtproto_request import MTProtoRequest


class GetFullChannelRequest(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    channels.getFullChannel#08736a09 channel:InputChannel = messages.ChatFull"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x8736a09

    def __init__(self, channel):
        """
        :param channel: Telegram type: InputChannel.
        """
        super(GetFullChannelRequest, self).__init__()
        self.result = None
        self.confirmed = True  # Confirmed by default

        self.channel = channel

    def on_send(self, writer):
        writer.write_int(GetFullChannelRequest.constructor_id, signed=False)
        self.channel.on_send(writer)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return GetFullChannelRequest(None)

    def on_response(self, reader):
        self.result = reader.tgread_object()

    def __repr__(self):
        return 'channels.getFullChannel#08736a09 channel:InputChannel = messages.ChatFull'

    def __str__(self):
        return '(channels.getFullChannel (ID: 0x8736a09) = (channel={}))'.format(str(self.channel))
