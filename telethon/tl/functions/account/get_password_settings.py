from ....tl.mtproto_request import MTProtoRequest


class GetPasswordSettingsRequest(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    account.getPasswordSettings#bc8d11bb current_password_hash:bytes = account.PasswordSettings"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0xbc8d11bb

    def __init__(self, current_password_hash):
        """
        :param current_password_hash: Telegram type: bytes.
        """
        super(GetPasswordSettingsRequest, self).__init__()
        self.result = None
        self.confirmed = True  # Confirmed by default

        self.current_password_hash = current_password_hash

    def on_send(self, writer):
        writer.write_int(GetPasswordSettingsRequest.constructor_id, signed=False)
        writer.tgwrite_bytes(self.current_password_hash)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return GetPasswordSettingsRequest(None)

    def on_response(self, reader):
        self.result = reader.tgread_object()

    def __repr__(self):
        return 'account.getPasswordSettings#bc8d11bb current_password_hash:bytes = account.PasswordSettings'

    def __str__(self):
        return '(account.getPasswordSettings (ID: 0xbc8d11bb) = (current_password_hash={}))'.format(str(self.current_password_hash))
