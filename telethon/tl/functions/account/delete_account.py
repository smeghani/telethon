from ....tl.mtproto_request import MTProtoRequest


class DeleteAccountRequest(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    account.deleteAccount#418d4e0b reason:string = Bool"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x418d4e0b

    def __init__(self, reason):
        """
        :param reason: Telegram type: string.
        """
        super(DeleteAccountRequest, self).__init__()
        self.result = None
        self.confirmed = True  # Confirmed by default

        self.reason = reason

    def on_send(self, writer):
        writer.write_int(DeleteAccountRequest.constructor_id, signed=False)
        writer.tgwrite_string(self.reason)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return DeleteAccountRequest(None)

    def on_response(self, reader):
        self.result = reader.tgread_object()

    def __repr__(self):
        return 'account.deleteAccount#418d4e0b reason:string = Bool'

    def __str__(self):
        return '(account.deleteAccount (ID: 0x418d4e0b) = (reason={}))'.format(str(self.reason))
