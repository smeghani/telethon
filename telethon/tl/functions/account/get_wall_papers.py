from ....tl.mtproto_request import MTProtoRequest


class GetWallPapersRequest(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    account.getWallPapers#c04cfac2  = Vector<WallPaper>"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0xc04cfac2

    def __init__(self):
        super(GetWallPapersRequest, self).__init__()
        self.result = None
        self.confirmed = True  # Confirmed by default

    def on_send(self, writer):
        writer.write_int(GetWallPapersRequest.constructor_id, signed=False)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return GetWallPapersRequest()

    def on_response(self, reader):
        self.result = reader.tgread_vector()

    def __repr__(self):
        return 'account.getWallPapers#c04cfac2  = Vector<WallPaper>'

    def __str__(self):
        return '(account.getWallPapers (ID: 0xc04cfac2) = ())'.format()
