from ....tl.mtproto_request import MTProtoRequest


class UpdateProfileRequest(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    account.updateProfile#78515775 flags:# first_name:flags.0?string last_name:flags.1?string about:flags.2?string = User"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x78515775

    def __init__(self, first_name=None, last_name=None, about=None):
        """
        :param first_name: Telegram type: string.
        :param last_name: Telegram type: string.
        :param about: Telegram type: string.
        """
        super(UpdateProfileRequest, self).__init__()
        self.result = None
        self.confirmed = True  # Confirmed by default

        self.first_name = first_name
        self.last_name = last_name
        self.about = about

    def on_send(self, writer):
        writer.write_int(UpdateProfileRequest.constructor_id, signed=False)
        # Calculate the flags. This equals to those flag arguments which are NOT None
        flags = 0
        flags |= (1 << 0) if self.first_name else 0
        flags |= (1 << 1) if self.last_name else 0
        flags |= (1 << 2) if self.about else 0
        writer.write_int(flags)

        if self.first_name:
            writer.tgwrite_string(self.first_name)

        if self.last_name:
            writer.tgwrite_string(self.last_name)

        if self.about:
            writer.tgwrite_string(self.about)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return UpdateProfileRequest(None, None, None)

    def on_response(self, reader):
        self.result = reader.tgread_object()

    def __repr__(self):
        return 'account.updateProfile#78515775 flags:# first_name:flags.0?string last_name:flags.1?string about:flags.2?string = User'

    def __str__(self):
        return '(account.updateProfile (ID: 0x78515775) = (first_name={}, last_name={}, about={}))'.format(str(self.first_name), str(self.last_name), str(self.about))
