from ....tl.mtproto_request import MTProtoRequest


class GetPinnedDialogsRequest(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    messages.getPinnedDialogs#e254d64e  = messages.PeerDialogs"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0xe254d64e

    def __init__(self):
        super(GetPinnedDialogsRequest, self).__init__()
        self.result = None
        self.confirmed = True  # Confirmed by default

    def on_send(self, writer):
        writer.write_int(GetPinnedDialogsRequest.constructor_id, signed=False)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return GetPinnedDialogsRequest()

    def on_response(self, reader):
        self.result = reader.tgread_object()

    def __repr__(self):
        return 'messages.getPinnedDialogs#e254d64e  = messages.PeerDialogs'

    def __str__(self):
        return '(messages.getPinnedDialogs (ID: 0xe254d64e) = ())'.format()
