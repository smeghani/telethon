from ....tl.mtproto_request import MTProtoRequest


class GetWebPageRequest(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    messages.getWebPage#32ca8f91 url:string hash:int = WebPage"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x32ca8f91

    def __init__(self, url, hash):
        """
        :param url: Telegram type: string.
        :param hash: Telegram type: int.
        """
        super(GetWebPageRequest, self).__init__()
        self.result = None
        self.confirmed = True  # Confirmed by default

        self.url = url
        self.hash = hash

    def on_send(self, writer):
        writer.write_int(GetWebPageRequest.constructor_id, signed=False)
        writer.tgwrite_string(self.url)
        writer.write_int(self.hash)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return GetWebPageRequest(None, None)

    def on_response(self, reader):
        self.result = reader.tgread_object()

    def __repr__(self):
        return 'messages.getWebPage#32ca8f91 url:string hash:int = WebPage'

    def __str__(self):
        return '(messages.getWebPage (ID: 0x32ca8f91) = (url={}, hash={}))'.format(str(self.url), str(self.hash))
