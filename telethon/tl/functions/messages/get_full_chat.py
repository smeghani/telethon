from ....tl.mtproto_request import MTProtoRequest


class GetFullChatRequest(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    messages.getFullChat#3b831c66 chat_id:int = messages.ChatFull"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x3b831c66

    def __init__(self, chat_id):
        """
        :param chat_id: Telegram type: int.
        """
        super(GetFullChatRequest, self).__init__()
        self.result = None
        self.confirmed = True  # Confirmed by default

        self.chat_id = chat_id

    def on_send(self, writer):
        writer.write_int(GetFullChatRequest.constructor_id, signed=False)
        writer.write_int(self.chat_id)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return GetFullChatRequest(None)

    def on_response(self, reader):
        self.result = reader.tgread_object()

    def __repr__(self):
        return 'messages.getFullChat#3b831c66 chat_id:int = messages.ChatFull'

    def __str__(self):
        return '(messages.getFullChat (ID: 0x3b831c66) = (chat_id={}))'.format(str(self.chat_id))
