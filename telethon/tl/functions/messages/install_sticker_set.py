from ....tl.mtproto_request import MTProtoRequest


class InstallStickerSetRequest(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    messages.installStickerSet#c78fe460 stickerset:InputStickerSet archived:Bool = messages.StickerSetInstallResult"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0xc78fe460

    def __init__(self, stickerset, archived):
        """
        :param stickerset: Telegram type: InputStickerSet.
        :param archived: Telegram type: Bool.
        """
        super(InstallStickerSetRequest, self).__init__()
        self.result = None
        self.confirmed = True  # Confirmed by default

        self.stickerset = stickerset
        self.archived = archived

    def on_send(self, writer):
        writer.write_int(InstallStickerSetRequest.constructor_id, signed=False)
        self.stickerset.on_send(writer)
        writer.tgwrite_bool(self.archived)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return InstallStickerSetRequest(None, None)

    def on_response(self, reader):
        self.result = reader.tgread_object()

    def __repr__(self):
        return 'messages.installStickerSet#c78fe460 stickerset:InputStickerSet archived:Bool = messages.StickerSetInstallResult'

    def __str__(self):
        return '(messages.installStickerSet (ID: 0xc78fe460) = (stickerset={}, archived={}))'.format(str(self.stickerset), str(self.archived))
