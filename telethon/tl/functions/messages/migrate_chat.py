from ....tl.mtproto_request import MTProtoRequest


class MigrateChatRequest(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    messages.migrateChat#15a3b8e3 chat_id:int = Updates"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x15a3b8e3

    def __init__(self, chat_id):
        """
        :param chat_id: Telegram type: int.
        """
        super(MigrateChatRequest, self).__init__()
        self.result = None
        self.confirmed = True  # Confirmed by default

        self.chat_id = chat_id

    def on_send(self, writer):
        writer.write_int(MigrateChatRequest.constructor_id, signed=False)
        writer.write_int(self.chat_id)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return MigrateChatRequest(None)

    def on_response(self, reader):
        self.result = reader.tgread_object()

    def __repr__(self):
        return 'messages.migrateChat#15a3b8e3 chat_id:int = Updates'

    def __str__(self):
        return '(messages.migrateChat (ID: 0x15a3b8e3) = (chat_id={}))'.format(str(self.chat_id))
