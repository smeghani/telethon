from ...tl.mtproto_request import MTProtoRequest


class DestroyAuthKeyRequest(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    destroy_auth_key#d1435160  = DestroyAuthKeyRes"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0xd1435160

    def __init__(self):
        super(DestroyAuthKeyRequest, self).__init__()
        self.result = None
        self.confirmed = True  # Confirmed by default

    def on_send(self, writer):
        writer.write_int(DestroyAuthKeyRequest.constructor_id, signed=False)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return DestroyAuthKeyRequest()

    def on_response(self, reader):
        self.result = reader.tgread_object()

    def __repr__(self):
        return 'destroy_auth_key#d1435160  = DestroyAuthKeyRes'

    def __str__(self):
        return '(destroy_auth_key (ID: 0xd1435160) = ())'.format()
