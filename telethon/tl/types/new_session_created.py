from ...tl.mtproto_request import MTProtoRequest


class NewSessionCreated(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    new_session_created#9ec20908 first_msg_id:long unique_id:long server_salt:long = NewSession"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x9ec20908

    def __init__(self, first_msg_id, unique_id, server_salt):
        """
        :param first_msg_id: Telegram type: long.
        :param unique_id: Telegram type: long.
        :param server_salt: Telegram type: long.
        """
        super(NewSessionCreated, self).__init__()

        self.first_msg_id = first_msg_id
        self.unique_id = unique_id
        self.server_salt = server_salt

    def on_send(self, writer):
        writer.write_int(NewSessionCreated.constructor_id, signed=False)
        writer.write_long(self.first_msg_id)
        writer.write_long(self.unique_id)
        writer.write_long(self.server_salt)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return NewSessionCreated(None, None, None)

    def on_response(self, reader):
        self.first_msg_id = reader.read_long()
        self.unique_id = reader.read_long()
        self.server_salt = reader.read_long()

    def __repr__(self):
        return 'new_session_created#9ec20908 first_msg_id:long unique_id:long server_salt:long = NewSession'

    def __str__(self):
        return '(new_session_created (ID: 0x9ec20908) = (first_msg_id={}, unique_id={}, server_salt={}))'.format(str(self.first_msg_id), str(self.unique_id), str(self.server_salt))
