from ...tl.mtproto_request import MTProtoRequest


class PageBlockSubheader(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    pageBlockSubheader#f12bb6e1 text:RichText = PageBlock"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0xf12bb6e1

    def __init__(self, text):
        """
        :param text: Telegram type: RichText.
        """
        super(PageBlockSubheader, self).__init__()

        self.text = text

    def on_send(self, writer):
        writer.write_int(PageBlockSubheader.constructor_id, signed=False)
        self.text.on_send(writer)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return PageBlockSubheader(None)

    def on_response(self, reader):
        self.text = reader.tgread_object()

    def __repr__(self):
        return 'pageBlockSubheader#f12bb6e1 text:RichText = PageBlock'

    def __str__(self):
        return '(pageBlockSubheader (ID: 0xf12bb6e1) = (text={}))'.format(str(self.text))
