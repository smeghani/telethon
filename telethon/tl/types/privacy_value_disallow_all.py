from ...tl.mtproto_request import MTProtoRequest


class PrivacyValueDisallowAll(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    privacyValueDisallowAll#8b73e763  = PrivacyRule"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x8b73e763

    def __init__(self):
        super(PrivacyValueDisallowAll, self).__init__()

    def on_send(self, writer):
        writer.write_int(PrivacyValueDisallowAll.constructor_id, signed=False)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return PrivacyValueDisallowAll()

    def on_response(self, reader):
        pass

    def __repr__(self):
        return 'privacyValueDisallowAll#8b73e763  = PrivacyRule'

    def __str__(self):
        return '(privacyValueDisallowAll (ID: 0x8b73e763) = ())'.format()
