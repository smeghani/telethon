from ...tl.mtproto_request import MTProtoRequest


class ChatInviteEmpty(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    chatInviteEmpty#69df3769  = ExportedChatInvite"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x69df3769

    def __init__(self):
        super(ChatInviteEmpty, self).__init__()

    def on_send(self, writer):
        writer.write_int(ChatInviteEmpty.constructor_id, signed=False)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return ChatInviteEmpty()

    def on_response(self, reader):
        pass

    def __repr__(self):
        return 'chatInviteEmpty#69df3769  = ExportedChatInvite'

    def __str__(self):
        return '(chatInviteEmpty (ID: 0x69df3769) = ())'.format()
