from ...tl.mtproto_request import MTProtoRequest


class WebPage(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    webPage#5f07b4bc flags:# id:long url:string display_url:string hash:int type:flags.0?string site_name:flags.1?string title:flags.2?string description:flags.3?string photo:flags.4?Photo embed_url:flags.5?string embed_type:flags.5?string embed_width:flags.6?int embed_height:flags.6?int duration:flags.7?int author:flags.8?string document:flags.9?Document cached_page:flags.10?Page = WebPage"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x5f07b4bc

    def __init__(self, id, url, display_url, hash, type=None, site_name=None, title=None, description=None, photo=None, embed_url=None, embed_type=None, embed_width=None, embed_height=None, duration=None, author=None, document=None, cached_page=None):
        """
        :param id: Telegram type: long.
        :param url: Telegram type: string.
        :param display_url: Telegram type: string.
        :param hash: Telegram type: int.
        :param type: Telegram type: string.
        :param site_name: Telegram type: string.
        :param title: Telegram type: string.
        :param description: Telegram type: string.
        :param photo: Telegram type: Photo.
        :param embed_url: Telegram type: string.
        :param embed_type: Telegram type: string.
        :param embed_width: Telegram type: int.
        :param embed_height: Telegram type: int.
        :param duration: Telegram type: int.
        :param author: Telegram type: string.
        :param document: Telegram type: Document.
        :param cached_page: Telegram type: Page.
        """
        super(WebPage, self).__init__()

        self.id = id
        self.url = url
        self.display_url = display_url
        self.hash = hash
        self.type = type
        self.site_name = site_name
        self.title = title
        self.description = description
        self.photo = photo
        self.embed_url = embed_url
        self.embed_type = embed_type
        self.embed_width = embed_width
        self.embed_height = embed_height
        self.duration = duration
        self.author = author
        self.document = document
        self.cached_page = cached_page

    def on_send(self, writer):
        writer.write_int(WebPage.constructor_id, signed=False)
        # Calculate the flags. This equals to those flag arguments which are NOT None
        flags = 0
        flags |= (1 << 0) if self.type else 0
        flags |= (1 << 1) if self.site_name else 0
        flags |= (1 << 2) if self.title else 0
        flags |= (1 << 3) if self.description else 0
        flags |= (1 << 4) if self.photo else 0
        flags |= (1 << 5) if self.embed_url else 0
        flags |= (1 << 5) if self.embed_type else 0
        flags |= (1 << 6) if self.embed_width else 0
        flags |= (1 << 6) if self.embed_height else 0
        flags |= (1 << 7) if self.duration else 0
        flags |= (1 << 8) if self.author else 0
        flags |= (1 << 9) if self.document else 0
        flags |= (1 << 10) if self.cached_page else 0
        writer.write_int(flags)

        writer.write_long(self.id)
        writer.tgwrite_string(self.url)
        writer.tgwrite_string(self.display_url)
        writer.write_int(self.hash)
        if self.type:
            writer.tgwrite_string(self.type)

        if self.site_name:
            writer.tgwrite_string(self.site_name)

        if self.title:
            writer.tgwrite_string(self.title)

        if self.description:
            writer.tgwrite_string(self.description)

        if self.photo:
            self.photo.on_send(writer)

        if self.embed_url:
            writer.tgwrite_string(self.embed_url)

        if self.embed_type:
            writer.tgwrite_string(self.embed_type)

        if self.embed_width:
            writer.write_int(self.embed_width)

        if self.embed_height:
            writer.write_int(self.embed_height)

        if self.duration:
            writer.write_int(self.duration)

        if self.author:
            writer.tgwrite_string(self.author)

        if self.document:
            self.document.on_send(writer)

        if self.cached_page:
            self.cached_page.on_send(writer)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return WebPage(None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None, None)

    def on_response(self, reader):
        flags = reader.read_int()

        self.id = reader.read_long()
        self.url = reader.tgread_string()
        self.display_url = reader.tgread_string()
        self.hash = reader.read_int()
        if (flags & (1 << 0)) != 0:
            self.type = reader.tgread_string()

        if (flags & (1 << 1)) != 0:
            self.site_name = reader.tgread_string()

        if (flags & (1 << 2)) != 0:
            self.title = reader.tgread_string()

        if (flags & (1 << 3)) != 0:
            self.description = reader.tgread_string()

        if (flags & (1 << 4)) != 0:
            self.photo = reader.tgread_object()

        if (flags & (1 << 5)) != 0:
            self.embed_url = reader.tgread_string()

        if (flags & (1 << 5)) != 0:
            self.embed_type = reader.tgread_string()

        if (flags & (1 << 6)) != 0:
            self.embed_width = reader.read_int()

        if (flags & (1 << 6)) != 0:
            self.embed_height = reader.read_int()

        if (flags & (1 << 7)) != 0:
            self.duration = reader.read_int()

        if (flags & (1 << 8)) != 0:
            self.author = reader.tgread_string()

        if (flags & (1 << 9)) != 0:
            self.document = reader.tgread_object()

        if (flags & (1 << 10)) != 0:
            self.cached_page = reader.tgread_object()

    def __repr__(self):
        return 'webPage#5f07b4bc flags:# id:long url:string display_url:string hash:int type:flags.0?string site_name:flags.1?string title:flags.2?string description:flags.3?string photo:flags.4?Photo embed_url:flags.5?string embed_type:flags.5?string embed_width:flags.6?int embed_height:flags.6?int duration:flags.7?int author:flags.8?string document:flags.9?Document cached_page:flags.10?Page = WebPage'

    def __str__(self):
        return '(webPage (ID: 0x5f07b4bc) = (id={}, url={}, display_url={}, hash={}, type={}, site_name={}, title={}, description={}, photo={}, embed_url={}, embed_type={}, embed_width={}, embed_height={}, duration={}, author={}, document={}, cached_page={}))'.format(str(self.id), str(self.url), str(self.display_url), str(self.hash), str(self.type), str(self.site_name), str(self.title), str(self.description), str(self.photo), str(self.embed_url), str(self.embed_type), str(self.embed_width), str(self.embed_height), str(self.duration), str(self.author), str(self.document), str(self.cached_page))
