from ...tl.mtproto_request import MTProtoRequest


class UpdateBotCallbackQuery(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    updateBotCallbackQuery#e73547e1 flags:# query_id:long user_id:int peer:Peer msg_id:int chat_instance:long data:flags.0?bytes game_short_name:flags.1?string = Update"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0xe73547e1

    def __init__(self, query_id, user_id, peer, msg_id, chat_instance, data=None, game_short_name=None):
        """
        :param query_id: Telegram type: long.
        :param user_id: Telegram type: int.
        :param peer: Telegram type: Peer.
        :param msg_id: Telegram type: int.
        :param chat_instance: Telegram type: long.
        :param data: Telegram type: bytes.
        :param game_short_name: Telegram type: string.
        """
        super(UpdateBotCallbackQuery, self).__init__()

        self.query_id = query_id
        self.user_id = user_id
        self.peer = peer
        self.msg_id = msg_id
        self.chat_instance = chat_instance
        self.data = data
        self.game_short_name = game_short_name

    def on_send(self, writer):
        writer.write_int(UpdateBotCallbackQuery.constructor_id, signed=False)
        # Calculate the flags. This equals to those flag arguments which are NOT None
        flags = 0
        flags |= (1 << 0) if self.data else 0
        flags |= (1 << 1) if self.game_short_name else 0
        writer.write_int(flags)

        writer.write_long(self.query_id)
        writer.write_int(self.user_id)
        self.peer.on_send(writer)
        writer.write_int(self.msg_id)
        writer.write_long(self.chat_instance)
        if self.data:
            writer.tgwrite_bytes(self.data)

        if self.game_short_name:
            writer.tgwrite_string(self.game_short_name)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return UpdateBotCallbackQuery(None, None, None, None, None, None, None)

    def on_response(self, reader):
        flags = reader.read_int()

        self.query_id = reader.read_long()
        self.user_id = reader.read_int()
        self.peer = reader.tgread_object()
        self.msg_id = reader.read_int()
        self.chat_instance = reader.read_long()
        if (flags & (1 << 0)) != 0:
            self.data = reader.tgread_bytes()

        if (flags & (1 << 1)) != 0:
            self.game_short_name = reader.tgread_string()

    def __repr__(self):
        return 'updateBotCallbackQuery#e73547e1 flags:# query_id:long user_id:int peer:Peer msg_id:int chat_instance:long data:flags.0?bytes game_short_name:flags.1?string = Update'

    def __str__(self):
        return '(updateBotCallbackQuery (ID: 0xe73547e1) = (query_id={}, user_id={}, peer={}, msg_id={}, chat_instance={}, data={}, game_short_name={}))'.format(str(self.query_id), str(self.user_id), str(self.peer), str(self.msg_id), str(self.chat_instance), str(self.data), str(self.game_short_name))
