from ...tl.mtproto_request import MTProtoRequest


class InputPhotoEmpty(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    inputPhotoEmpty#1cd7bf0d  = InputPhoto"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x1cd7bf0d

    def __init__(self):
        super(InputPhotoEmpty, self).__init__()

    def on_send(self, writer):
        writer.write_int(InputPhotoEmpty.constructor_id, signed=False)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return InputPhotoEmpty()

    def on_response(self, reader):
        pass

    def __repr__(self):
        return 'inputPhotoEmpty#1cd7bf0d  = InputPhoto'

    def __str__(self):
        return '(inputPhotoEmpty (ID: 0x1cd7bf0d) = ())'.format()
