from ...tl.mtproto_request import MTProtoRequest


class InputBotInlineResultDocument(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    inputBotInlineResultDocument#fff8fdc4 flags:# id:string type:string title:flags.1?string description:flags.2?string document:InputDocument send_message:InputBotInlineMessage = InputBotInlineResult"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0xfff8fdc4

    def __init__(self, id, type, document, send_message, title=None, description=None):
        """
        :param id: Telegram type: string.
        :param type: Telegram type: string.
        :param title: Telegram type: string.
        :param description: Telegram type: string.
        :param document: Telegram type: InputDocument.
        :param send_message: Telegram type: InputBotInlineMessage.
        """
        super(InputBotInlineResultDocument, self).__init__()

        self.id = id
        self.type = type
        self.title = title
        self.description = description
        self.document = document
        self.send_message = send_message

    def on_send(self, writer):
        writer.write_int(InputBotInlineResultDocument.constructor_id, signed=False)
        # Calculate the flags. This equals to those flag arguments which are NOT None
        flags = 0
        flags |= (1 << 1) if self.title else 0
        flags |= (1 << 2) if self.description else 0
        writer.write_int(flags)

        writer.tgwrite_string(self.id)
        writer.tgwrite_string(self.type)
        if self.title:
            writer.tgwrite_string(self.title)

        if self.description:
            writer.tgwrite_string(self.description)

        self.document.on_send(writer)
        self.send_message.on_send(writer)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return InputBotInlineResultDocument(None, None, None, None, None, None)

    def on_response(self, reader):
        flags = reader.read_int()

        self.id = reader.tgread_string()
        self.type = reader.tgread_string()
        if (flags & (1 << 1)) != 0:
            self.title = reader.tgread_string()

        if (flags & (1 << 2)) != 0:
            self.description = reader.tgread_string()

        self.document = reader.tgread_object()
        self.send_message = reader.tgread_object()

    def __repr__(self):
        return 'inputBotInlineResultDocument#fff8fdc4 flags:# id:string type:string title:flags.1?string description:flags.2?string document:InputDocument send_message:InputBotInlineMessage = InputBotInlineResult'

    def __str__(self):
        return '(inputBotInlineResultDocument (ID: 0xfff8fdc4) = (id={}, type={}, title={}, description={}, document={}, send_message={}))'.format(str(self.id), str(self.type), str(self.title), str(self.description), str(self.document), str(self.send_message))
