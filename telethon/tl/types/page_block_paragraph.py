from ...tl.mtproto_request import MTProtoRequest


class PageBlockParagraph(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    pageBlockParagraph#467a0766 text:RichText = PageBlock"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x467a0766

    def __init__(self, text):
        """
        :param text: Telegram type: RichText.
        """
        super(PageBlockParagraph, self).__init__()

        self.text = text

    def on_send(self, writer):
        writer.write_int(PageBlockParagraph.constructor_id, signed=False)
        self.text.on_send(writer)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return PageBlockParagraph(None)

    def on_response(self, reader):
        self.text = reader.tgread_object()

    def __repr__(self):
        return 'pageBlockParagraph#467a0766 text:RichText = PageBlock'

    def __str__(self):
        return '(pageBlockParagraph (ID: 0x467a0766) = (text={}))'.format(str(self.text))
