from ...tl.mtproto_request import MTProtoRequest


class UpdateChannelPinnedMessage(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    updateChannelPinnedMessage#98592475 channel_id:int id:int = Update"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x98592475

    def __init__(self, channel_id, id):
        """
        :param channel_id: Telegram type: int.
        :param id: Telegram type: int.
        """
        super(UpdateChannelPinnedMessage, self).__init__()

        self.channel_id = channel_id
        self.id = id

    def on_send(self, writer):
        writer.write_int(UpdateChannelPinnedMessage.constructor_id, signed=False)
        writer.write_int(self.channel_id)
        writer.write_int(self.id)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return UpdateChannelPinnedMessage(None, None)

    def on_response(self, reader):
        self.channel_id = reader.read_int()
        self.id = reader.read_int()

    def __repr__(self):
        return 'updateChannelPinnedMessage#98592475 channel_id:int id:int = Update'

    def __str__(self):
        return '(updateChannelPinnedMessage (ID: 0x98592475) = (channel_id={}, id={}))'.format(str(self.channel_id), str(self.id))
