from ...tl.mtproto_request import MTProtoRequest


class WebDocument(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    webDocument#c61acbd8 url:string access_hash:long size:int mime_type:string attributes:Vector<DocumentAttribute> dc_id:int = WebDocument"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0xc61acbd8

    def __init__(self, url, access_hash, size, mime_type, attributes, dc_id):
        """
        :param url: Telegram type: string.
        :param access_hash: Telegram type: long.
        :param size: Telegram type: int.
        :param mime_type: Telegram type: string.
        :param attributes: Telegram type: DocumentAttribute. Must be a list.
        :param dc_id: Telegram type: int.
        """
        super(WebDocument, self).__init__()

        self.url = url
        self.access_hash = access_hash
        self.size = size
        self.mime_type = mime_type
        self.attributes = attributes
        self.dc_id = dc_id

    def on_send(self, writer):
        writer.write_int(WebDocument.constructor_id, signed=False)
        writer.tgwrite_string(self.url)
        writer.write_long(self.access_hash)
        writer.write_int(self.size)
        writer.tgwrite_string(self.mime_type)
        writer.write_int(0x1cb5c415, signed=False)  # Vector's constructor ID
        writer.write_int(len(self.attributes))
        for attributes_item in self.attributes:
            attributes_item.on_send(writer)

        writer.write_int(self.dc_id)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return WebDocument(None, None, None, None, None, None)

    def on_response(self, reader):
        self.url = reader.tgread_string()
        self.access_hash = reader.read_long()
        self.size = reader.read_int()
        self.mime_type = reader.tgread_string()
        reader.read_int()  # Vector's constructor ID
        self.attributes = []  # Initialize an empty list
        attributes_len = reader.read_int()
        for _ in range(attributes_len):
            attributes_item = reader.tgread_object()
            self.attributes.append(attributes_item)

        self.dc_id = reader.read_int()

    def __repr__(self):
        return 'webDocument#c61acbd8 url:string access_hash:long size:int mime_type:string attributes:Vector<DocumentAttribute> dc_id:int = WebDocument'

    def __str__(self):
        return '(webDocument (ID: 0xc61acbd8) = (url={}, access_hash={}, size={}, mime_type={}, attributes={}, dc_id={}))'.format(str(self.url), str(self.access_hash), str(self.size), str(self.mime_type), None if not self.attributes else [str(_) for _ in self.attributes], str(self.dc_id))
