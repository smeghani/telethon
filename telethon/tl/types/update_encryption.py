from ...tl.mtproto_request import MTProtoRequest


class UpdateEncryption(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    updateEncryption#b4a2e88d chat:EncryptedChat date:date = Update"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0xb4a2e88d

    def __init__(self, chat, date):
        """
        :param chat: Telegram type: EncryptedChat.
        :param date: Telegram type: date.
        """
        super(UpdateEncryption, self).__init__()

        self.chat = chat
        self.date = date

    def on_send(self, writer):
        writer.write_int(UpdateEncryption.constructor_id, signed=False)
        self.chat.on_send(writer)
        writer.tgwrite_date(self.date)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return UpdateEncryption(None, None)

    def on_response(self, reader):
        self.chat = reader.tgread_object()
        self.date = reader.tgread_date()

    def __repr__(self):
        return 'updateEncryption#b4a2e88d chat:EncryptedChat date:date = Update'

    def __str__(self):
        return '(updateEncryption (ID: 0xb4a2e88d) = (chat={}, date={}))'.format(str(self.chat), str(self.date))
