from ...tl.mtproto_request import MTProtoRequest


class SendMessageTypingAction(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    sendMessageTypingAction#16bf744e  = SendMessageAction"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x16bf744e

    def __init__(self):
        super(SendMessageTypingAction, self).__init__()

    def on_send(self, writer):
        writer.write_int(SendMessageTypingAction.constructor_id, signed=False)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return SendMessageTypingAction()

    def on_response(self, reader):
        pass

    def __repr__(self):
        return 'sendMessageTypingAction#16bf744e  = SendMessageAction'

    def __str__(self):
        return '(sendMessageTypingAction (ID: 0x16bf744e) = ())'.format()
