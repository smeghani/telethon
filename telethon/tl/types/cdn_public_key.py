from ...tl.mtproto_request import MTProtoRequest


class CdnPublicKey(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    cdnPublicKey#c982eaba dc_id:int public_key:string = CdnPublicKey"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0xc982eaba

    def __init__(self, dc_id, public_key):
        """
        :param dc_id: Telegram type: int.
        :param public_key: Telegram type: string.
        """
        super(CdnPublicKey, self).__init__()

        self.dc_id = dc_id
        self.public_key = public_key

    def on_send(self, writer):
        writer.write_int(CdnPublicKey.constructor_id, signed=False)
        writer.write_int(self.dc_id)
        writer.tgwrite_string(self.public_key)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return CdnPublicKey(None, None)

    def on_response(self, reader):
        self.dc_id = reader.read_int()
        self.public_key = reader.tgread_string()

    def __repr__(self):
        return 'cdnPublicKey#c982eaba dc_id:int public_key:string = CdnPublicKey'

    def __str__(self):
        return '(cdnPublicKey (ID: 0xc982eaba) = (dc_id={}, public_key={}))'.format(str(self.dc_id), str(self.public_key))
