from ...tl.mtproto_request import MTProtoRequest


class PageBlockChannel(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    pageBlockChannel#ef1751b5 channel:Chat = PageBlock"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0xef1751b5

    def __init__(self, channel):
        """
        :param channel: Telegram type: Chat.
        """
        super(PageBlockChannel, self).__init__()

        self.channel = channel

    def on_send(self, writer):
        writer.write_int(PageBlockChannel.constructor_id, signed=False)
        self.channel.on_send(writer)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return PageBlockChannel(None)

    def on_response(self, reader):
        self.channel = reader.tgread_object()

    def __repr__(self):
        return 'pageBlockChannel#ef1751b5 channel:Chat = PageBlock'

    def __str__(self):
        return '(pageBlockChannel (ID: 0xef1751b5) = (channel={}))'.format(str(self.channel))
