from ....tl.mtproto_request import MTProtoRequest


class NoAppUpdate(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    help.noAppUpdate#c45a6536  = help.AppUpdate"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0xc45a6536

    def __init__(self):
        super(NoAppUpdate, self).__init__()

    def on_send(self, writer):
        writer.write_int(NoAppUpdate.constructor_id, signed=False)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return NoAppUpdate()

    def on_response(self, reader):
        pass

    def __repr__(self):
        return 'help.noAppUpdate#c45a6536  = help.AppUpdate'

    def __str__(self):
        return '(help.noAppUpdate (ID: 0xc45a6536) = ())'.format()
