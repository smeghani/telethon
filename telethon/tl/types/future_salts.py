from ...tl.mtproto_request import MTProtoRequest


class FutureSalts(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    future_salts#ae500895 req_msg_id:long now:int salts:vector<future_salt> = FutureSalts"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0xae500895

    def __init__(self, req_msg_id, now, salts):
        """
        :param req_msg_id: Telegram type: long.
        :param now: Telegram type: int.
        :param salts: Telegram type: future_salt. Must be a list.
        """
        super(FutureSalts, self).__init__()

        self.req_msg_id = req_msg_id
        self.now = now
        self.salts = salts

    def on_send(self, writer):
        writer.write_int(FutureSalts.constructor_id, signed=False)
        writer.write_long(self.req_msg_id)
        writer.write_int(self.now)
        writer.write_int(len(self.salts))
        for salts_item in self.salts:
            salts_item.on_send(writer)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return FutureSalts(None, None, None)

    def on_response(self, reader):
        self.req_msg_id = reader.read_long()
        self.now = reader.read_int()
        self.salts = []  # Initialize an empty list
        salts_len = reader.read_int()
        for _ in range(salts_len):
            salts_item = reader.tgread_object()
            self.salts.append(salts_item)

    def __repr__(self):
        return 'future_salts#ae500895 req_msg_id:long now:int salts:vector<future_salt> = FutureSalts'

    def __str__(self):
        return '(future_salts (ID: 0xae500895) = (req_msg_id={}, now={}, salts={}))'.format(str(self.req_msg_id), str(self.now), None if not self.salts else [str(_) for _ in self.salts])
