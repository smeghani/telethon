from ...tl.mtproto_request import MTProtoRequest


class WallPaperSolid(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    wallPaperSolid#63117f24 id:int title:string bg_color:int color:int = WallPaper"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x63117f24

    def __init__(self, id, title, bg_color, color):
        """
        :param id: Telegram type: int.
        :param title: Telegram type: string.
        :param bg_color: Telegram type: int.
        :param color: Telegram type: int.
        """
        super(WallPaperSolid, self).__init__()

        self.id = id
        self.title = title
        self.bg_color = bg_color
        self.color = color

    def on_send(self, writer):
        writer.write_int(WallPaperSolid.constructor_id, signed=False)
        writer.write_int(self.id)
        writer.tgwrite_string(self.title)
        writer.write_int(self.bg_color)
        writer.write_int(self.color)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return WallPaperSolid(None, None, None, None)

    def on_response(self, reader):
        self.id = reader.read_int()
        self.title = reader.tgread_string()
        self.bg_color = reader.read_int()
        self.color = reader.read_int()

    def __repr__(self):
        return 'wallPaperSolid#63117f24 id:int title:string bg_color:int color:int = WallPaper'

    def __str__(self):
        return '(wallPaperSolid (ID: 0x63117f24) = (id={}, title={}, bg_color={}, color={}))'.format(str(self.id), str(self.title), str(self.bg_color), str(self.color))
