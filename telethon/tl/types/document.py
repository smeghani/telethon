from ...tl.mtproto_request import MTProtoRequest


class Document(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    document#87232bc7 id:long access_hash:long date:date mime_type:string size:int thumb:PhotoSize dc_id:int version:int attributes:Vector<DocumentAttribute> = Document"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x87232bc7

    def __init__(self, id, access_hash, date, mime_type, size, thumb, dc_id, version, attributes):
        """
        :param id: Telegram type: long.
        :param access_hash: Telegram type: long.
        :param date: Telegram type: date.
        :param mime_type: Telegram type: string.
        :param size: Telegram type: int.
        :param thumb: Telegram type: PhotoSize.
        :param dc_id: Telegram type: int.
        :param version: Telegram type: int.
        :param attributes: Telegram type: DocumentAttribute. Must be a list.
        """
        super(Document, self).__init__()

        self.id = id
        self.access_hash = access_hash
        self.date = date
        self.mime_type = mime_type
        self.size = size
        self.thumb = thumb
        self.dc_id = dc_id
        self.version = version
        self.attributes = attributes

    def on_send(self, writer):
        writer.write_int(Document.constructor_id, signed=False)
        writer.write_long(self.id)
        writer.write_long(self.access_hash)
        writer.tgwrite_date(self.date)
        writer.tgwrite_string(self.mime_type)
        writer.write_int(self.size)
        self.thumb.on_send(writer)
        writer.write_int(self.dc_id)
        writer.write_int(self.version)
        writer.write_int(0x1cb5c415, signed=False)  # Vector's constructor ID
        writer.write_int(len(self.attributes))
        for attributes_item in self.attributes:
            attributes_item.on_send(writer)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return Document(None, None, None, None, None, None, None, None, None)

    def on_response(self, reader):
        self.id = reader.read_long()
        self.access_hash = reader.read_long()
        self.date = reader.tgread_date()
        self.mime_type = reader.tgread_string()
        self.size = reader.read_int()
        self.thumb = reader.tgread_object()
        self.dc_id = reader.read_int()
        self.version = reader.read_int()
        reader.read_int()  # Vector's constructor ID
        self.attributes = []  # Initialize an empty list
        attributes_len = reader.read_int()
        for _ in range(attributes_len):
            attributes_item = reader.tgread_object()
            self.attributes.append(attributes_item)

    def __repr__(self):
        return 'document#87232bc7 id:long access_hash:long date:date mime_type:string size:int thumb:PhotoSize dc_id:int version:int attributes:Vector<DocumentAttribute> = Document'

    def __str__(self):
        return '(document (ID: 0x87232bc7) = (id={}, access_hash={}, date={}, mime_type={}, size={}, thumb={}, dc_id={}, version={}, attributes={}))'.format(str(self.id), str(self.access_hash), str(self.date), str(self.mime_type), str(self.size), str(self.thumb), str(self.dc_id), str(self.version), None if not self.attributes else [str(_) for _ in self.attributes])
