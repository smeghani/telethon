from ...tl.mtproto_request import MTProtoRequest


class InputPrivacyValueDisallowUsers(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    inputPrivacyValueDisallowUsers#90110467 users:Vector<InputUser> = InputPrivacyRule"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x90110467

    def __init__(self, users):
        """
        :param users: Telegram type: InputUser. Must be a list.
        """
        super(InputPrivacyValueDisallowUsers, self).__init__()

        self.users = users

    def on_send(self, writer):
        writer.write_int(InputPrivacyValueDisallowUsers.constructor_id, signed=False)
        writer.write_int(0x1cb5c415, signed=False)  # Vector's constructor ID
        writer.write_int(len(self.users))
        for users_item in self.users:
            users_item.on_send(writer)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return InputPrivacyValueDisallowUsers(None)

    def on_response(self, reader):
        reader.read_int()  # Vector's constructor ID
        self.users = []  # Initialize an empty list
        users_len = reader.read_int()
        for _ in range(users_len):
            users_item = reader.tgread_object()
            self.users.append(users_item)

    def __repr__(self):
        return 'inputPrivacyValueDisallowUsers#90110467 users:Vector<InputUser> = InputPrivacyRule'

    def __str__(self):
        return '(inputPrivacyValueDisallowUsers (ID: 0x90110467) = (users={}))'.format(None if not self.users else [str(_) for _ in self.users])
