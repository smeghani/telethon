from ...tl.mtproto_request import MTProtoRequest


class InputMessageEntityMentionName(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    inputMessageEntityMentionName#208e68c9 offset:int length:int user_id:InputUser = MessageEntity"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x208e68c9

    def __init__(self, offset, length, user_id):
        """
        :param offset: Telegram type: int.
        :param length: Telegram type: int.
        :param user_id: Telegram type: InputUser.
        """
        super(InputMessageEntityMentionName, self).__init__()

        self.offset = offset
        self.length = length
        self.user_id = user_id

    def on_send(self, writer):
        writer.write_int(InputMessageEntityMentionName.constructor_id, signed=False)
        writer.write_int(self.offset)
        writer.write_int(self.length)
        self.user_id.on_send(writer)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return InputMessageEntityMentionName(None, None, None)

    def on_response(self, reader):
        self.offset = reader.read_int()
        self.length = reader.read_int()
        self.user_id = reader.tgread_object()

    def __repr__(self):
        return 'inputMessageEntityMentionName#208e68c9 offset:int length:int user_id:InputUser = MessageEntity'

    def __str__(self):
        return '(inputMessageEntityMentionName (ID: 0x208e68c9) = (offset={}, length={}, user_id={}))'.format(str(self.offset), str(self.length), str(self.user_id))
