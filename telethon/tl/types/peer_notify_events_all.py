from ...tl.mtproto_request import MTProtoRequest


class PeerNotifyEventsAll(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    peerNotifyEventsAll#6d1ded88  = PeerNotifyEvents"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x6d1ded88

    def __init__(self):
        super(PeerNotifyEventsAll, self).__init__()

    def on_send(self, writer):
        writer.write_int(PeerNotifyEventsAll.constructor_id, signed=False)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return PeerNotifyEventsAll()

    def on_response(self, reader):
        pass

    def __repr__(self):
        return 'peerNotifyEventsAll#6d1ded88  = PeerNotifyEvents'

    def __str__(self):
        return '(peerNotifyEventsAll (ID: 0x6d1ded88) = ())'.format()
