from ...tl.mtproto_request import MTProtoRequest


class DhGenRetry(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    dh_gen_retry#46dc1fb9 nonce:int128 server_nonce:int128 new_nonce_hash2:int128 = Set_client_DH_params_answer"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x46dc1fb9

    def __init__(self, nonce, server_nonce, new_nonce_hash2):
        """
        :param nonce: Telegram type: int128.
        :param server_nonce: Telegram type: int128.
        :param new_nonce_hash2: Telegram type: int128.
        """
        super(DhGenRetry, self).__init__()

        self.nonce = nonce
        self.server_nonce = server_nonce
        self.new_nonce_hash2 = new_nonce_hash2

    def on_send(self, writer):
        writer.write_int(DhGenRetry.constructor_id, signed=False)
        writer.write_large_int(self.nonce, bits=128)
        writer.write_large_int(self.server_nonce, bits=128)
        writer.write_large_int(self.new_nonce_hash2, bits=128)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return DhGenRetry(None, None, None)

    def on_response(self, reader):
        self.nonce = reader.read_large_int(bits=128)
        self.server_nonce = reader.read_large_int(bits=128)
        self.new_nonce_hash2 = reader.read_large_int(bits=128)

    def __repr__(self):
        return 'dh_gen_retry#46dc1fb9 nonce:int128 server_nonce:int128 new_nonce_hash2:int128 = Set_client_DH_params_answer'

    def __str__(self):
        return '(dh_gen_retry (ID: 0x46dc1fb9) = (nonce={}, server_nonce={}, new_nonce_hash2={}))'.format(str(self.nonce), str(self.server_nonce), str(self.new_nonce_hash2))
