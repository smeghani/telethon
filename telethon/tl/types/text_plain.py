from ...tl.mtproto_request import MTProtoRequest


class TextPlain(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    textPlain#744694e0 text:string = RichText"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x744694e0

    def __init__(self, text):
        """
        :param text: Telegram type: string.
        """
        super(TextPlain, self).__init__()

        self.text = text

    def on_send(self, writer):
        writer.write_int(TextPlain.constructor_id, signed=False)
        writer.tgwrite_string(self.text)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return TextPlain(None)

    def on_response(self, reader):
        self.text = reader.tgread_string()

    def __repr__(self):
        return 'textPlain#744694e0 text:string = RichText'

    def __str__(self):
        return '(textPlain (ID: 0x744694e0) = (text={}))'.format(str(self.text))
