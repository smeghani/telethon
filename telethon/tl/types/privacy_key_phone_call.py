from ...tl.mtproto_request import MTProtoRequest


class PrivacyKeyPhoneCall(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    privacyKeyPhoneCall#3d662b7b  = PrivacyKey"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x3d662b7b

    def __init__(self):
        super(PrivacyKeyPhoneCall, self).__init__()

    def on_send(self, writer):
        writer.write_int(PrivacyKeyPhoneCall.constructor_id, signed=False)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return PrivacyKeyPhoneCall()

    def on_response(self, reader):
        pass

    def __repr__(self):
        return 'privacyKeyPhoneCall#3d662b7b  = PrivacyKey'

    def __str__(self):
        return '(privacyKeyPhoneCall (ID: 0x3d662b7b) = ())'.format()
