from ...tl.mtproto_request import MTProtoRequest


class InputPeerEmpty(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    inputPeerEmpty#7f3b18ea  = InputPeer"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x7f3b18ea

    def __init__(self):
        super(InputPeerEmpty, self).__init__()

    def on_send(self, writer):
        writer.write_int(InputPeerEmpty.constructor_id, signed=False)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return InputPeerEmpty()

    def on_response(self, reader):
        pass

    def __repr__(self):
        return 'inputPeerEmpty#7f3b18ea  = InputPeer'

    def __str__(self):
        return '(inputPeerEmpty (ID: 0x7f3b18ea) = ())'.format()
