from ...tl.mtproto_request import MTProtoRequest


class KeyboardButtonBuy(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    keyboardButtonBuy#afd93fbb text:string = KeyboardButton"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0xafd93fbb

    def __init__(self, text):
        """
        :param text: Telegram type: string.
        """
        super(KeyboardButtonBuy, self).__init__()

        self.text = text

    def on_send(self, writer):
        writer.write_int(KeyboardButtonBuy.constructor_id, signed=False)
        writer.tgwrite_string(self.text)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return KeyboardButtonBuy(None)

    def on_response(self, reader):
        self.text = reader.tgread_string()

    def __repr__(self):
        return 'keyboardButtonBuy#afd93fbb text:string = KeyboardButton'

    def __str__(self):
        return '(keyboardButtonBuy (ID: 0xafd93fbb) = (text={}))'.format(str(self.text))
