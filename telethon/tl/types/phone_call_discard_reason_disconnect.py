from ...tl.mtproto_request import MTProtoRequest


class PhoneCallDiscardReasonDisconnect(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    phoneCallDiscardReasonDisconnect#e095c1a0  = PhoneCallDiscardReason"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0xe095c1a0

    def __init__(self):
        super(PhoneCallDiscardReasonDisconnect, self).__init__()

    def on_send(self, writer):
        writer.write_int(PhoneCallDiscardReasonDisconnect.constructor_id, signed=False)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return PhoneCallDiscardReasonDisconnect()

    def on_response(self, reader):
        pass

    def __repr__(self):
        return 'phoneCallDiscardReasonDisconnect#e095c1a0  = PhoneCallDiscardReason'

    def __str__(self):
        return '(phoneCallDiscardReasonDisconnect (ID: 0xe095c1a0) = ())'.format()
