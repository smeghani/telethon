from ...tl.mtproto_request import MTProtoRequest


class ChannelParticipantsRecent(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    channelParticipantsRecent#de3f3c79  = ChannelParticipantsFilter"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0xde3f3c79

    def __init__(self):
        super(ChannelParticipantsRecent, self).__init__()

    def on_send(self, writer):
        writer.write_int(ChannelParticipantsRecent.constructor_id, signed=False)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return ChannelParticipantsRecent()

    def on_response(self, reader):
        pass

    def __repr__(self):
        return 'channelParticipantsRecent#de3f3c79  = ChannelParticipantsFilter'

    def __str__(self):
        return '(channelParticipantsRecent (ID: 0xde3f3c79) = ())'.format()
