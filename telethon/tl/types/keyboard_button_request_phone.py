from ...tl.mtproto_request import MTProtoRequest


class KeyboardButtonRequestPhone(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    keyboardButtonRequestPhone#b16a6c29 text:string = KeyboardButton"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0xb16a6c29

    def __init__(self, text):
        """
        :param text: Telegram type: string.
        """
        super(KeyboardButtonRequestPhone, self).__init__()

        self.text = text

    def on_send(self, writer):
        writer.write_int(KeyboardButtonRequestPhone.constructor_id, signed=False)
        writer.tgwrite_string(self.text)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return KeyboardButtonRequestPhone(None)

    def on_response(self, reader):
        self.text = reader.tgread_string()

    def __repr__(self):
        return 'keyboardButtonRequestPhone#b16a6c29 text:string = KeyboardButton'

    def __str__(self):
        return '(keyboardButtonRequestPhone (ID: 0xb16a6c29) = (text={}))'.format(str(self.text))
