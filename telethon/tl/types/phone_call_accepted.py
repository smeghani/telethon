from ...tl.mtproto_request import MTProtoRequest


class PhoneCallAccepted(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    phoneCallAccepted#6d003d3f id:long access_hash:long date:date admin_id:int participant_id:int g_b:bytes protocol:PhoneCallProtocol = PhoneCall"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x6d003d3f

    def __init__(self, id, access_hash, date, admin_id, participant_id, g_b, protocol):
        """
        :param id: Telegram type: long.
        :param access_hash: Telegram type: long.
        :param date: Telegram type: date.
        :param admin_id: Telegram type: int.
        :param participant_id: Telegram type: int.
        :param g_b: Telegram type: bytes.
        :param protocol: Telegram type: PhoneCallProtocol.
        """
        super(PhoneCallAccepted, self).__init__()

        self.id = id
        self.access_hash = access_hash
        self.date = date
        self.admin_id = admin_id
        self.participant_id = participant_id
        self.g_b = g_b
        self.protocol = protocol

    def on_send(self, writer):
        writer.write_int(PhoneCallAccepted.constructor_id, signed=False)
        writer.write_long(self.id)
        writer.write_long(self.access_hash)
        writer.tgwrite_date(self.date)
        writer.write_int(self.admin_id)
        writer.write_int(self.participant_id)
        writer.tgwrite_bytes(self.g_b)
        self.protocol.on_send(writer)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return PhoneCallAccepted(None, None, None, None, None, None, None)

    def on_response(self, reader):
        self.id = reader.read_long()
        self.access_hash = reader.read_long()
        self.date = reader.tgread_date()
        self.admin_id = reader.read_int()
        self.participant_id = reader.read_int()
        self.g_b = reader.tgread_bytes()
        self.protocol = reader.tgread_object()

    def __repr__(self):
        return 'phoneCallAccepted#6d003d3f id:long access_hash:long date:date admin_id:int participant_id:int g_b:bytes protocol:PhoneCallProtocol = PhoneCall'

    def __str__(self):
        return '(phoneCallAccepted (ID: 0x6d003d3f) = (id={}, access_hash={}, date={}, admin_id={}, participant_id={}, g_b={}, protocol={}))'.format(str(self.id), str(self.access_hash), str(self.date), str(self.admin_id), str(self.participant_id), str(self.g_b), str(self.protocol))
