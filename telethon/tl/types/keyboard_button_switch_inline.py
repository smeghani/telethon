from ...tl.mtproto_request import MTProtoRequest


class KeyboardButtonSwitchInline(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    keyboardButtonSwitchInline#0568a748 flags:# same_peer:flags.0?true text:string query:string = KeyboardButton"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x568a748

    def __init__(self, text, query, same_peer=None):
        """
        :param same_peer: Telegram type: true.
        :param text: Telegram type: string.
        :param query: Telegram type: string.
        """
        super(KeyboardButtonSwitchInline, self).__init__()

        self.same_peer = same_peer
        self.text = text
        self.query = query

    def on_send(self, writer):
        writer.write_int(KeyboardButtonSwitchInline.constructor_id, signed=False)
        # Calculate the flags. This equals to those flag arguments which are NOT None
        flags = 0
        flags |= (1 << 0) if self.same_peer else 0
        writer.write_int(flags)

        writer.tgwrite_string(self.text)
        writer.tgwrite_string(self.query)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return KeyboardButtonSwitchInline(None, None, None)

    def on_response(self, reader):
        flags = reader.read_int()

        if (flags & (1 << 0)) != 0:
            self.same_peer = True  # Arbitrary not-None value, no need to read since it is a flag

        self.text = reader.tgread_string()
        self.query = reader.tgread_string()

    def __repr__(self):
        return 'keyboardButtonSwitchInline#0568a748 flags:# same_peer:flags.0?true text:string query:string = KeyboardButton'

    def __str__(self):
        return '(keyboardButtonSwitchInline (ID: 0x568a748) = (same_peer={}, text={}, query={}))'.format(str(self.same_peer), str(self.text), str(self.query))
