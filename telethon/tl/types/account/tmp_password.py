from ....tl.mtproto_request import MTProtoRequest


class TmpPassword(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    account.tmpPassword#db64fd34 tmp_password:bytes valid_until:int = account.TmpPassword"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0xdb64fd34

    def __init__(self, tmp_password, valid_until):
        """
        :param tmp_password: Telegram type: bytes.
        :param valid_until: Telegram type: int.
        """
        super(TmpPassword, self).__init__()

        self.tmp_password = tmp_password
        self.valid_until = valid_until

    def on_send(self, writer):
        writer.write_int(TmpPassword.constructor_id, signed=False)
        writer.tgwrite_bytes(self.tmp_password)
        writer.write_int(self.valid_until)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return TmpPassword(None, None)

    def on_response(self, reader):
        self.tmp_password = reader.tgread_bytes()
        self.valid_until = reader.read_int()

    def __repr__(self):
        return 'account.tmpPassword#db64fd34 tmp_password:bytes valid_until:int = account.TmpPassword'

    def __str__(self):
        return '(account.tmpPassword (ID: 0xdb64fd34) = (tmp_password={}, valid_until={}))'.format(str(self.tmp_password), str(self.valid_until))
