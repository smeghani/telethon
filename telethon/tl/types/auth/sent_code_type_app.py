from ....tl.mtproto_request import MTProtoRequest


class SentCodeTypeApp(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    auth.sentCodeTypeApp#3dbb5986 length:int = auth.SentCodeType"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x3dbb5986

    def __init__(self, length):
        """
        :param length: Telegram type: int.
        """
        super(SentCodeTypeApp, self).__init__()

        self.length = length

    def on_send(self, writer):
        writer.write_int(SentCodeTypeApp.constructor_id, signed=False)
        writer.write_int(self.length)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return SentCodeTypeApp(None)

    def on_response(self, reader):
        self.length = reader.read_int()

    def __repr__(self):
        return 'auth.sentCodeTypeApp#3dbb5986 length:int = auth.SentCodeType'

    def __str__(self):
        return '(auth.sentCodeTypeApp (ID: 0x3dbb5986) = (length={}))'.format(str(self.length))
