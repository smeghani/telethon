from ....tl.mtproto_request import MTProtoRequest


class PasswordRecovery(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    auth.passwordRecovery#137948a5 email_pattern:string = auth.PasswordRecovery"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x137948a5

    def __init__(self, email_pattern):
        """
        :param email_pattern: Telegram type: string.
        """
        super(PasswordRecovery, self).__init__()

        self.email_pattern = email_pattern

    def on_send(self, writer):
        writer.write_int(PasswordRecovery.constructor_id, signed=False)
        writer.tgwrite_string(self.email_pattern)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return PasswordRecovery(None)

    def on_response(self, reader):
        self.email_pattern = reader.tgread_string()

    def __repr__(self):
        return 'auth.passwordRecovery#137948a5 email_pattern:string = auth.PasswordRecovery'

    def __str__(self):
        return '(auth.passwordRecovery (ID: 0x137948a5) = (email_pattern={}))'.format(str(self.email_pattern))
