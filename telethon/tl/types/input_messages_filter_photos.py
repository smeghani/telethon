from ...tl.mtproto_request import MTProtoRequest


class InputMessagesFilterPhotos(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    inputMessagesFilterPhotos#9609a51c  = MessagesFilter"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x9609a51c

    def __init__(self):
        super(InputMessagesFilterPhotos, self).__init__()

    def on_send(self, writer):
        writer.write_int(InputMessagesFilterPhotos.constructor_id, signed=False)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return InputMessagesFilterPhotos()

    def on_response(self, reader):
        pass

    def __repr__(self):
        return 'inputMessagesFilterPhotos#9609a51c  = MessagesFilter'

    def __str__(self):
        return '(inputMessagesFilterPhotos (ID: 0x9609a51c) = ())'.format()
