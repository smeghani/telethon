from ...tl.mtproto_request import MTProtoRequest


class ReplyInlineMarkup(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    replyInlineMarkup#48a30254 rows:Vector<KeyboardButtonRow> = ReplyMarkup"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x48a30254

    def __init__(self, rows):
        """
        :param rows: Telegram type: KeyboardButtonRow. Must be a list.
        """
        super(ReplyInlineMarkup, self).__init__()

        self.rows = rows

    def on_send(self, writer):
        writer.write_int(ReplyInlineMarkup.constructor_id, signed=False)
        writer.write_int(0x1cb5c415, signed=False)  # Vector's constructor ID
        writer.write_int(len(self.rows))
        for rows_item in self.rows:
            rows_item.on_send(writer)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return ReplyInlineMarkup(None)

    def on_response(self, reader):
        reader.read_int()  # Vector's constructor ID
        self.rows = []  # Initialize an empty list
        rows_len = reader.read_int()
        for _ in range(rows_len):
            rows_item = reader.tgread_object()
            self.rows.append(rows_item)

    def __repr__(self):
        return 'replyInlineMarkup#48a30254 rows:Vector<KeyboardButtonRow> = ReplyMarkup'

    def __str__(self):
        return '(replyInlineMarkup (ID: 0x48a30254) = (rows={}))'.format(None if not self.rows else [str(_) for _ in self.rows])
