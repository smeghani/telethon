from ...tl.mtproto_request import MTProtoRequest


class DestroyAuthKeyNone(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    destroy_auth_key_none#0a9f2259  = DestroyAuthKeyRes"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0xa9f2259

    def __init__(self):
        super(DestroyAuthKeyNone, self).__init__()

    def on_send(self, writer):
        writer.write_int(DestroyAuthKeyNone.constructor_id, signed=False)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return DestroyAuthKeyNone()

    def on_response(self, reader):
        pass

    def __repr__(self):
        return 'destroy_auth_key_none#0a9f2259  = DestroyAuthKeyRes'

    def __str__(self):
        return '(destroy_auth_key_none (ID: 0xa9f2259) = ())'.format()
