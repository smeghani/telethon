from ...tl.mtproto_request import MTProtoRequest


class ChannelParticipantsBots(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    channelParticipantsBots#b0d1865b  = ChannelParticipantsFilter"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0xb0d1865b

    def __init__(self):
        super(ChannelParticipantsBots, self).__init__()

    def on_send(self, writer):
        writer.write_int(ChannelParticipantsBots.constructor_id, signed=False)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return ChannelParticipantsBots()

    def on_response(self, reader):
        pass

    def __repr__(self):
        return 'channelParticipantsBots#b0d1865b  = ChannelParticipantsFilter'

    def __str__(self):
        return '(channelParticipantsBots (ID: 0xb0d1865b) = ())'.format()
