from ....tl.mtproto_request import MTProtoRequest


class CdnFile(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    upload.cdnFile#a99fca4f bytes:bytes = upload.CdnFile"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0xa99fca4f

    def __init__(self, bytes):
        """
        :param bytes: Telegram type: bytes.
        """
        super(CdnFile, self).__init__()

        self.bytes = bytes

    def on_send(self, writer):
        writer.write_int(CdnFile.constructor_id, signed=False)
        writer.tgwrite_bytes(self.bytes)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return CdnFile(None)

    def on_response(self, reader):
        self.bytes = reader.tgread_bytes()

    def __repr__(self):
        return 'upload.cdnFile#a99fca4f bytes:bytes = upload.CdnFile'

    def __str__(self):
        return '(upload.cdnFile (ID: 0xa99fca4f) = (bytes={}))'.format(str(self.bytes))
