from ....tl.mtproto_request import MTProtoRequest


class File(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    upload.file#096a18d5 type:storage.FileType mtime:int bytes:bytes = upload.File"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0x96a18d5

    def __init__(self, type, mtime, bytes):
        """
        :param type: Telegram type: storage.FileType.
        :param mtime: Telegram type: int.
        :param bytes: Telegram type: bytes.
        """
        super(File, self).__init__()

        self.type = type
        self.mtime = mtime
        self.bytes = bytes

    def on_send(self, writer):
        writer.write_int(File.constructor_id, signed=False)
        self.type.on_send(writer)
        writer.write_int(self.mtime)
        writer.tgwrite_bytes(self.bytes)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return File(None, None, None)

    def on_response(self, reader):
        self.type = reader.tgread_object()
        self.mtime = reader.read_int()
        self.bytes = reader.tgread_bytes()

    def __repr__(self):
        return 'upload.file#096a18d5 type:storage.FileType mtime:int bytes:bytes = upload.File'

    def __str__(self):
        return '(upload.file (ID: 0x96a18d5) = (type={}, mtime={}, bytes={}))'.format(str(self.type), str(self.mtime), str(self.bytes))
