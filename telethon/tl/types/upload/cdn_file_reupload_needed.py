from ....tl.mtproto_request import MTProtoRequest


class CdnFileReuploadNeeded(MTProtoRequest):
    """Class generated by TLObjects' generator. All changes will be ERASED. Original .tl definition below.
    upload.cdnFileReuploadNeeded#eea8e46e request_token:bytes = upload.CdnFile"""

    # Telegram's constructor ID (and unique identifier) for this class
    constructor_id = 0xeea8e46e

    def __init__(self, request_token):
        """
        :param request_token: Telegram type: bytes.
        """
        super(CdnFileReuploadNeeded, self).__init__()

        self.request_token = request_token

    def on_send(self, writer):
        writer.write_int(CdnFileReuploadNeeded.constructor_id, signed=False)
        writer.tgwrite_bytes(self.request_token)

    @staticmethod
    def empty():
        """Returns an "empty" instance (all attributes are None)"""
        return CdnFileReuploadNeeded(None)

    def on_response(self, reader):
        self.request_token = reader.tgread_bytes()

    def __repr__(self):
        return 'upload.cdnFileReuploadNeeded#eea8e46e request_token:bytes = upload.CdnFile'

    def __str__(self):
        return '(upload.cdnFileReuploadNeeded (ID: 0xeea8e46e) = (request_token={}))'.format(str(self.request_token))
